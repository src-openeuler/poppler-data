Name:                   poppler-data
Version:                0.4.12
Release:                1
Summary:                Encoding Files for use with libpoppler
License:                BSD and GPLv2	
URL:                    https://poppler.freedesktop.org	
Source0:                https://poppler.freedesktop.org/%{name}-%{version}.tar.gz
  
BUildArch:              noarch

%description
Poppler is a PDF rendering library based on the xpdf-3.0 code base.

%package                devel
Summary:                Development files for poppler-data
Requires:               %{name} = %{version}-%{release}
BuildRequires:          pkgconfig

%description            devel
This package is the development files for popper-data.

%prep
%autosetup -n %{name}-%{version} -p1 

%build
%make_build prefix=%{_prefix}

%install
%make_install prefix=%{_prefix}

%files
%defattr(-,root,root)
%license COPYING*
%doc README
%{_datadir}/poppler/

%files devel
%defattr(-,root,root)
%{_datadir}/pkgconfig/poppler-data.pc


%changelog
* Wed Dec 27 2023 konglidong <konglidong@uniontech.com> - 0.4.12-1
- update version to 0.4.12

* Tue Aug 02 2022 tianlijing <tianlijing@kylinos.cn> - 0.4.11-1
- update to 0.4.11

* Mon Jan 20 2020 zhanzhimin <zhanzhimin@huawei.com> - 0.4.10-1
- update to 0.4.10

* Thu Sep 5 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.9-4
-Type:enhancement
-Id:NA
-SUG:NA
-DESC:Increase the ability of encoding files for use with libpoppler 

* Mon Aug 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.9-3
- Package init

